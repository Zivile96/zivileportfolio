require('colors');

var prevColor = 0;
var line = '';

for (var i = 1; i <= 7; i++) {
    var tempCount = i;
    while(tempCount > 0) {
        line += '#'; 
        tempCount--;
    }
    var randColor = Math.floor(Math.random()*4)+1;
    while(randColor === prevColor) {
        randColor = Math.floor(Math.random()*4)+1;
    }
    prevColor = randColor;
    switch(randColor) {
        case 1:
            console.log(line.yellow);
            break;
        case 2:
            console.log(line.red);
            break;
        case 3:
            console.log(line.green);
            break;
        case 4:
            console.log(line.blue);
            break;       
    }
}