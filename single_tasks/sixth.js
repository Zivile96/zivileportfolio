function removeDuplicates(arr){
    for(var i = 0; i < arr.length; i++) {
        while(arr.indexOf(arr[i], i+1) > 0) {
            arr.splice(arr.indexOf(arr[i],i+1), 1);
        }
    }
    return arr;
}

console.log(removeDuplicates([1,2,5,1,2,3,2,5]));