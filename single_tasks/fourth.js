// without prototype
function asum(arr){
    var sum = 0;
    for(var i = 0; i < arr.length; i++){
        sum += arr[i];
    }
    return sum;
}

function bsum(arr){
    var res = arr.reduce(function(a, b){
        return a + b;
    });
    return res;
}

console.log(asum([1,2,2,4]));
console.log(bsum([1,2,3,5]));