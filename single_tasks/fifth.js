var houses = [
    {name: "Targaryen", motto: "Fire and Blood"},
    {name: "Stark", motto: "Winter is Coming"},
    {name: "Bolton", motto: "Our Blades Are Sharp"},
    {name: "Greyjoy", motto: "We Do Not Sow"},
    {name: "Tully", motto: "Family, Duty, Honor"},
    {name: "Arryn", motto: "As High as Honor"},
    {name: "Lannister", motto: "Hear Me Roar!"},
    {name: "Tyrell", motto: "Growing Strong"},
    {name: "Baratheon", motto: "Ours is the Fury"},
    {name: "Martell", motto: "Unbowed, Unbent, Unbroken"}
    ];

//ES5
function search(name) {

    for (var i = 0; i < houses.length; i++){
        if(houses[i].name === name){
            return houses[i].motto;
        }
    }
}

//ES6
function search6(name) {

    var temp = houses.filter(obj => obj.name === name);
    return temp[0].motto;
}

console.log(search('Bolton'));
console.log(search6('Stark'));