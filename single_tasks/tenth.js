
function spy(func) {

    var countCalls = 0;
    
    var res = function(text){
        countCalls++;
        return func.apply(this, arguments);
    }

    res.report = function() {
        return countCalls;
    }

    return res;
}

function printLine(value) {
    console.log(value);
}

var spied = spy(printLine);
spied('smth');
spied('smth2');
spied('cat');
console.log(spied.report());





