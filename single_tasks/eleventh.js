function calcSum (arr) {
    
    var sum = 0;

    for (var i = 0; i < arr.length; i++) {
        
        if (arr[i].length !== undefined) {

            sum += calcSum(arr[i]);

        }
        else sum += arr[i];
    }
    return sum;
}

console.log(calcSum([10, 6, [4, 8], 3, [6, 5, [9, [4, 4]]]]));