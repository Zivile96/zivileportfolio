class calculator {

    constructor(startValue) {
        this.value = startValue;
    }

    add(value) {
        this.value += value;
        return this;
    }

    substract(value) {
        this.value -= value;
        return this;
    }
    
    multiply(value) {
        this.value *= value;
        return this;
    }

    divide(value) {
        this.value /= value;
        return this;
    }

    valueOf() {
        return this.value;
    }
}

var calc = new calculator(2);
var amount = calc.add(2).substract(3).multiply(6).divide(3).valueOf();
console.log(amount);