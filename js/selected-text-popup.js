document.addEventListener('DOMContentLoaded', function(){

	(function(){
		var popup_box = document.getElementById('popup-box');
        
		function clearText() {
			if (window.getSelection){
				if (window.getSelection().empty){
					window.getSelection().empty();
				} else if (window.getSelection().removeAllRanges){
					window.getSelection().removeAllRanges();
				}
			} else if (document.selection){
				document.selection.empty();
			}
		}

		document.addEventListener('mouseup', function(event) {

			var text = document.getSelection().toString();

			if(!text){
				popup_box.classList.add('hidden');

			} else if(text){
				popup_box.classList.remove('hidden');

				popup_box.style.left = event.x+'px';
				popup_box.style.top = event.y+'px';
			}

			document.querySelector('.fb-select').addEventListener('click', function(){
			});
		});

		document.addEventListener('mousedown', (event) => {
			var text = document.getSelection().toString();
			if (!popup_box.classList.contains('hidden') && text.length > 1 && !event.target.classList.contains('share-popup') ) {
				popup_box.classList.add('hidden');
				clearText();
			}
		});

	}());
});