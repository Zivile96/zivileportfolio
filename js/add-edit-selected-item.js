document.addEventListener('DOMContentLoaded', function(){

	var fetch_url = 'http://localhost:3000/items/';

	load_content(fetch_url);

});

function load_content(fetch_url){

	var url = window.location;
	var params = new URL(url).searchParams;
	var parse_id = params.get('id');

	var content = '';
	var preview_window = document.getElementById('preview-window');

	fetch(fetch_url).then(function(response){
		response.json().then(function(data){

			var id;
			for (var i = 0; i < data.length; i++){
				if(data[i].id == parse_id){
					id = i;
				}
			}

			var does_parse_id_exist = false;
			for(i = 0; i < data.length; i++){
				if(data[i].id == parse_id){ //with === doesnt work
					does_parse_id_exist = true;
				}
			}
			if(!does_parse_id_exist && parse_id != 'new'){
				window.location.href = 'error-page.html';
			}

			var isNew = parse_id == 'new';
			content = 
                `<div class="preview-container">
                    <h3>Name:</h3>
                    <input type="text" id="name-input" value=${isNew ? '' : data[id].name}>
                    <h3>Item:</h3>
                    <input type="item" id="item-input" value=${isNew ? '' : data[id].item}>
                    <button type="button" class="change-item-button">OK</button>
                </div>`;

			preview_window.innerHTML += content;
			change_button_event(parse_id, fetch_url);
		});            
	});
}

function change_button_event(parse_id, fetch_url){
	document.querySelector('.change-item-button').addEventListener('click', function(){
		//validation
		var item = document.getElementById('item-input');
		var name = document.getElementById('name-input');
		if (!item.value || !name.value){
			item.value ? item.style.backgroundColor = '#fff' : item.style.backgroundColor = '#ff0000';
			name.value ? name .style.backgroundColor = '#fff': name.style.backgroundColor = '#ff0000';
		} else if(parse_id != 'new'){
			change_item(fetch_url+parse_id, parse_id, {id: parse_id, name: name.value, item: item.value});
		} else{
			add_new_item(fetch_url, {name: name.value, item: item.value});
		}
	});
}

function change_item(url, id, data_change){

	fetch(url, {
		method: 'PUT',
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify(data_change)
	})  
		.then(function(){
			window.location.href = 'ranking.html';
		});
}

function add_new_item(url, data_add){

	fetch(url, {
		method: 'POST',
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify(data_add)
	}).then(function(){
		window.location.href = 'ranking.html';
	});

}