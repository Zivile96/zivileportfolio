
document.addEventListener('DOMContentLoaded', function() {

	var url = 'http://localhost:3000/items';
	var filter_input_field = document.querySelector('#filter-input');
	var sort_select_fields = document.querySelectorAll('.sort-select');
	var sort_input_field = document.querySelector('#sort-select-input');
	var asc_desc_value = document.querySelector('#sort-asc-desc-input');

	load_data(url);

	document.querySelector('#add-item-button').addEventListener('click', function(){

		window.location.href = 'preview.html?id=new';
	});
    
	filter_input_field.addEventListener('input', function(){
		filter_by_name(objects_arr, filter_input_field.value);
	});

	sort_select_fields.forEach(function(field){
		field.addEventListener('input', function(){
			set_sort_storage(sort_input_field.value, asc_desc_value.value);
			sort_by_value(objects_arr, sort_input_field.value, asc_desc_value.value);
		});
	});
});

function load_data(url){

	var sort_input_field = document.querySelector('#sort-select-input');
	var asc_desc_value = document.querySelector('#sort-asc-desc-input');
	var row = '';
	objects_arr = [];

	var table = document.getElementById('table-content');

	fetch(url).then(function(response){
		response.json().then(function(data){
			data.forEach(item => {
				objects_arr.push({id: item.id, name: item.name, item: item.item});
				row += 
                `<div class="rows" id="row-${item.id}">
                    <div> ${item.id} </div> 
                    <div id="name-value"> ${item.name}  </div> 
                    <div> ${item.item}  </div>
                    <button type="button" onclick="location.href='preview.html?id=${item.id}'" id="edit-${item.id}" class="edit-button">Edit</button> 
                    <button type="button" onclick="delete_item(${item.id})" id="delete-${item.id}" class="delete-button">Delete</button> 
                </div>`;
			});
			table.innerHTML += row;

			var sort_values = get_sort_storage();
			document.querySelector('#sort-select-input').value = sort_values.sort_input;
			document.querySelector('#sort-asc-desc-input').value = sort_values.asc_desc;
			sort_by_value(data, sort_input_field.value, asc_desc_value.value);
		});
	});
}

function delete_item(id){

	document.querySelector(`#delete-${id}`).disabled = true;
	var url = 'http://localhost:3000/items';

	fetch(url + `/${id}`, {
		method: 'DELETE'
	}).then(response => {
		if(response.status == 200){
			document.querySelector(`#row-${id}`).remove();
			delete_object_by_id(objects_arr, id);
		}
	});
}

function delete_object_by_id(objects_arr, id){

	for(var i = 0; i < objects_arr.length; i++){
		if(objects_arr[i].id == id){
			objects_arr.splice(i, 1);
		}
	}
}

function filter_by_name(objects_arr, filter_value){

	if(filter_value == ''){
		objects_arr.forEach((row) => document.getElementById(`row-${row.id}`).classList.remove('hidden'));
	}

	else{
		for(var i=0; i<objects_arr.length; i++){
			var row =  document.getElementById(`row-${objects_arr[i].id}`);
			var items = [...row.querySelectorAll('div')].map(element => element.innerHTML.toLowerCase()).join(' ');
			if(items.includes(`${filter_value}`)) {
				row.classList.remove('hidden');
			} else {
				row.classList.add('hidden');
			}
		}
	}
}

function sort_by_value(objects_arr, sort_input_field, asc_desc_value){

	if (sort_input_field == 'ID'){
		objects_arr.sort(function(val1, val2){
			return asc_desc_value === 'asc' ? val1.id-val2.id : val2.id-val1.id;
		}); 
	} else {
		objects_arr.sort(function(val1 ,val2){
			var nameA = val1[sort_input_field].toUpperCase();
			var nameB = val2[sort_input_field].toUpperCase(); 
			if (nameA < nameB) {
				return asc_desc_value === 'asc' ? -1 : 1;
			}
			if (nameA > nameB) {
				return asc_desc_value === 'asc' ? 1 : -1;
			}
			return 0;
		}); 
	}

	for (var i=0; i<objects_arr.length; i++){
		var row =  document.getElementById(`row-${objects_arr[i].id}`);
		row.style.order = i;
	}
}

function set_sort_storage(sort_input, asc_desc){

	localStorage.setItem('sort_input', sort_input);
	localStorage.setItem('asc_desc', asc_desc);
}

function get_sort_storage(){

	var sort_input = localStorage.getItem('sort_input') || 'ID';
	var asc_desc = localStorage.getItem('asc_desc') || 'asc';

	return {'sort_input': sort_input, 'asc_desc': asc_desc};
}