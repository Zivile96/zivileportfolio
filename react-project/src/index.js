import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button } from 'reactstrap';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MoviePage from './moviePage';

const Index = () => (
    <div>
        <h1>Welcome to movies page</h1>
        
    </div>
)

export default Index

var movieCardStyle = {
    width: '300px'
}

const MovieList = (props) => (
    <div className="col-4">
        <Card>
            <CardImg style={movieCardStyle} src={props.image} alt="Card image cap" />
            <CardBody>
                <CardTitle>{props.title}</CardTitle>
                <CardSubtitle>Year: {props.year}</CardSubtitle>
                <CardText>
                    Rating: {props.rating}
                </CardText>                  
                    <Button>View</Button>
                    <Link to={`/movie`}>Link</Link>
            </CardBody>
        </Card>
    </div>
);

class Movies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: []
        };
    }

    render() {
        return(
        <div>
          
            <div className="row">
                {   
                    this.state.movies.map( movie =><MovieList {...movie}/> )
                }
            </div>
        </div>
        );
    }

    componentDidMount () {
        fetch("http://localhost:3001/movies")
        .then(response => response.json())
        .then(data => this.setState({ movies: data }))
    }   
}

class Form extends React.Component {
    buttonName = "Select";

    render() {
        return (
            <div>
                <input placeholder="Type in Movie"/>
                <button>{this.buttonName}</button>
            </div>
        );
    }
}

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path = '/' component={Movies}/>
                    <Route path = '/movie' component={MoviePage}/>
                </div>
            </Router>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
